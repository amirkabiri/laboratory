export default function(phone){
    return /^09[0-9]{9}$/.test(phone);
}