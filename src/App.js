import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

import HomePage from './Pages/Home/Home';

import ContactIndex from './Pages/Contact/index';
import ContactPrint from './Pages/Contact/Print';
import ContactPhone from './Pages/Contact/Phone';
import ContactSmsSent from './Pages/Contact/SmsSent';

import ReservationHome from './Pages/Reservation/Home';
import ReservationReserve from './Pages/Reservation/Reserve';
import ReservationResult from './Pages/Reservation/Result';
import ReservationPhone from './Pages/Reservation/Phone';
import ReservationDone from './Pages/Reservation/Done';

import GuideHome from './Pages/Guide/Home';
import GuidePrinted from './Pages/Guide/Printed';
import GuidePhone from './Pages/Guide/Phone';
import GuideShowItem from './Pages/Guide/ShowItem';
import GuideScanReserveCode from './Pages/Guide/ScanReserveCode';
import GuideManualReserveCode from './Pages/Guide/ManualReserveCode';
import GuideDone from './Pages/Guide/Done';

import ExperimentResultScan from './Pages/ExperimentResult/Scan';
import ExperimentResultManualReserve from './Pages/ExperimentResult/ManualReserve';
import ExperimentResultNotReady from './Pages/ExperimentResult/NotReady';
import ExperimentResultCheckout from './Pages/ExperimentResult/Checkout';
import ExperimentResultResult from './Pages/ExperimentResult/Result';
import ExperimentResultPhone from './Pages/ExperimentResult/Phone';
import ExperimentResultSmsSent from './Pages/ExperimentResult/SmsSent';
import ExperimentResultPrinted from './Pages/ExperimentResult/Printed';

import SurveyIndex from './Pages/Survey/index';
import SurveyDone from './Pages/Survey/SurveyDone';
import SurveyComplaint from './Pages/Survey/Complaint';
import SurveyPhone from './Pages/Survey/Phone';
import SurveySmsSent from './Pages/Survey/SmsSent';

import CheckoutScan from './Pages/Checkout/Scan';
import CheckoutAlreadyCheckout from './Pages/Checkout/AlreadyCheckout';
import CheckoutReserve from './Pages/Checkout/Reserve';
import CheckoutCheckout from './Pages/Checkout/Checkout';
import CheckoutError from './Pages/Checkout/Error';
import CheckoutSuccess from './Pages/Checkout/Success';

import ReceptionIndex from './Pages/Reception/Index';
import ReceptionBirthDate from './Pages/Reception/BirthDate';
import ReceptionCheckout from './Pages/Reception/Checkout';
import ReceptionDone from './Pages/Reception/Done';
import ReceptionName from './Pages/Reception/Name';
import ReceptionNationalCode from './Pages/Reception/NationalCode';
import ReceptionPhone from './Pages/Reception/Phone';
import ReceptionStaticPhone from './Pages/Reception/StaticPhone';


const routes = [
    {
        path : '/',
        component : HomePage,
    },

    {
        path : '/reception',
        component : ReceptionIndex
    },{
        path : '/reception/birth-date',
        component : ReceptionBirthDate
    },{
        path : '/reception/checkout',
        component : ReceptionCheckout
    },{
        path : '/reception/done',
        component : ReceptionDone
    },{
        path : '/reception/name',
        component : ReceptionName
    },{
        path : '/reception/national-code',
        component : ReceptionNationalCode
    },{
        path : '/reception/phone',
        component : ReceptionPhone
    },{
        path : '/reception/static-phone',
        component : ReceptionStaticPhone
    },

    {
        path : '/contact',
        component : ContactIndex,
    },{
        path : '/contact/print',
        component : ContactPrint,
    },{
        path : '/contact/phone',
        component : ContactPhone,
    },{
        path : '/contact/sms-sent',
        component : ContactSmsSent,
    },

    {
        path : '/reservation',
        component : ReservationHome
    },{
        path : '/reservation/reserve',
        component : ReservationReserve
    },{
        path : '/reservation/result',
        component : ReservationResult
    },{
        path : '/reservation/phone',
        component : ReservationPhone
    },{
        path : '/reservation/done',
        component : ReservationDone
    },

    {
        path : '/guide',
        component : GuideHome
    },{
        path : '/guide/printed',
        component : GuidePrinted
    },{
        path : '/guide/phone',
        component : GuidePhone
    },{
        path : '/guide/item',
        component : GuideShowItem
    },{
        path : '/guide/scan',
        component : GuideScanReserveCode
    },{
        path : '/guide/manual',
        component : GuideManualReserveCode
    },{
        path : '/guide/done',
        component : GuideDone
    },

    {
        path : '/experiment-result/scan',
        component : ExperimentResultScan
    },{
        path : '/experiment-result/reserve',
        component : ExperimentResultManualReserve
    },{
        path : '/experiment-result/not-ready',
        component : ExperimentResultNotReady
    },{
        path : '/experiment-result/checkout',
        component : ExperimentResultCheckout
    },{
        path : '/experiment-result/result',
        component : ExperimentResultResult
    },{
        path : '/experiment-result/phone',
        component : ExperimentResultPhone
    },{
        path : '/experiment-result/sms-sent',
        component : ExperimentResultSmsSent
    },{
        path : '/experiment-result/printed',
        component : ExperimentResultPrinted
    },

    {
        path : '/survey',
        component : SurveyIndex
    },{
        path : '/survey/done',
        component : SurveyDone
    },{
        path : '/survey/complaint',
        component : SurveyComplaint
    },{
        path : '/survey/phone',
        component : SurveyPhone
    },{
        path : '/survey/sms-sent',
        component : SurveySmsSent
    },

    {
        path : '/checkout/scan',
        component : CheckoutScan
    },{
        path : '/checkout/already-checkout',
        component : CheckoutAlreadyCheckout
    },{
        path : '/checkout/reserve',
        component : CheckoutReserve
    },{
        path : '/checkout/checkout',
        component : CheckoutCheckout
    },{
        path : '/checkout/success',
        component : CheckoutSuccess
    },{
        path : '/checkout/error',
        component : CheckoutError
    },
];

function App(){
    return (
        <Router>
            <Switch>
                {
                    routes.map((route, index) => (
                        <Route key={ index } path={ route.path } component={ route.component } exact={ route.exact || true }/>
                    ))
                }
            </Switch>
        </Router>
    );
}

export default App;