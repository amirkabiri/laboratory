import React from 'react';
import EnterPhonePage from "../../Components/EnterPhonePage";

export default function Phone({ history }){
    return (
        <EnterPhonePage
            onSubmit={ () => history.push('/contact/sms-sent') }
        />
    );
}