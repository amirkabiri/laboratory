import React from 'react';
import BluePage from 'Components/BluePage/BluePage';
import styles from './index.module.css';
import classNames from 'classnames';

import InstagramIcon from 'Assets/img/instagram-icon.svg';
import FacebookIcon from 'Assets/img/facebook-icon.svg';
import TwitterIcon from 'Assets/img/twitter-icon.svg';
import SmsCard from "Components/SmsCard/SmsCard";
import PrintCard from "Components/PrintCard/PrintCard";

export default function Index(){
    return (
        <BluePage>
            <div className="container">
                <h1 className="header-title">اطلاعات تماس</h1>
                <p className={ styles.description }>آزمایشگاه و پاتولوژی فوق تخصصی پرینا آماده خدمتگذاری به شما عزیزان میباشد</p>

                <div className={ classNames('card', styles.card) }>
                    <div className={ styles.addresses }>
                        <p>تهران، سعادت آباد، میدان بهرود، خیابان آذرمهر</p>
                        <p>تلفن : 02177900</p>
                        <p>نمابر : 02177900566</p>
                        <p>ایمیل : hello@parnialaboratory.com</p>
                    </div>

                    <div className={ styles.socialNetworks }>
                        <div>
                            <span>parnia_laboratory_tehran</span>
                            <img src={ InstagramIcon }/>
                        </div>
                        <div>
                            <span>parnia_laboratory</span>
                            <img src={ FacebookIcon }/>
                        </div>
                        <div>
                            <span>parnia_laboratory</span>
                            <img src={ TwitterIcon }/>
                        </div>
                    </div>
                </div>

                <SmsCard
                    style={{ marginBottom : 20 }}
                    href="/contact/phone"
                />

                <PrintCard
                    style={{ marginBottom : 60 }}
                    href="/contact/print"
                />
            </div>
        </BluePage>
    );
}