import React from 'react';
import SmsSentPage from "../../Components/SmsSentPage/SmsSentPage";

export default function SmsSent(){
    const phoneNumber = '09146878528';

    return (
        <SmsSentPage
            description={ `جواب آزمایش به شماره موبایل ${ phoneNumber } پیامک شد.` }
            title="پیامک ارسال شد"
        />
    )
}