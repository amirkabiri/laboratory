import React from 'react';
import PrintedPage from "../../Components/PrintedPage";
import SmsCard from "../../Components/SmsCard/SmsCard";

export default function Print(){
    return (
        <PrintedPage
            title="اطلاعات بر روی کاغذ چاپ شد"
        >
            <SmsCard
                description="با وارد کردن شماره موبایل خود لینک اطلاعات آماگی قبل تست را دریافت کنید"
                style={{ padding : 50 }}
                href="/contact/phone"
            />
        </PrintedPage>
    );
}