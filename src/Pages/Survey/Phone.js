import React, {useRef, useState} from 'react';
import BluePage from "../../Components/BluePage/BluePage";
import SuccessIcon from 'Assets/img/success-icon.svg';
import styles from './Phone.module.scss';
import Input from "../../Components/Input/Input";
import NumericKeyboard from "../../Components/NumericKeyboard/NumericKeyboard";
import Button from "../../Components/Button/Button";
import classNames from 'classnames';
import validatePhone from '../../Shared/validatePhone';

export default function Phone({ history }){
    const phoneNumberRef = useRef();
    const [touched, setTouched] = useState(false);
    const [invalid, setInvalid] = useState(false);

    const onSubmit = () => {
        const { value } = phoneNumberRef.current;

        if(!validatePhone(value)){
            setTouched(true);
            setInvalid(true);
            return;
        }

        history.push('/survey/sms-sent');
    };

    return (
        <BluePage>
            <div className={ classNames('container', styles.container) }>
                <img className={ styles.icon } src={ SuccessIcon } alt="success icon"/>
                <h1 className={ styles.title }>شکایت شما ارسال شد و توسط مدیریت بررسی خواهد شد</h1>
                <p className={ styles.description }>شما میتوانید شماره همراه خود را جهت پیگیری های بعدی وارد نمایید</p>
                <Input
                    ref={ phoneNumberRef }
                    placeholder="شماره همراه"
                    style={{ margin : '40px 0 0 0' }}
                    autoFocus={ true }
                    invalid={ touched && invalid }
                />
                <div className="flex justify-content-center" style={{ marginTop : 40 }}>
                    <NumericKeyboard
                        inputRef={ phoneNumberRef }
                    />
                </div>

                <div className="flex justify-content-center" style={{ marginTop : 40 }}>
                    <Button
                        disabled={ false }
                        onClick={ onSubmit }
                        color="success"
                    >تایید</Button>
                </div>
            </div>
        </BluePage>
    )
}