import React, {useState} from 'react';
import Header from "../../Components/Header/Header";
import styles from './Complaint.module.scss';
import classNames from 'classnames';
import SelectBox from "../../Components/SelectBox";
import Keyboard from "../../Components/Keyboard/Keyboard";

export default function Complaint({ history }){
    const [textarea, setTextarea] = useState('');

    const onKeyboardChange = e => {
        switch (e.type) {
            case 'char':
                setTextarea(textarea => textarea + e.value);
                break;

            case 'escape':
                setTextarea(textarea => textarea.slice(0, textarea.length - 1));
                break;

            case 'search':
                history.push('/survey/phone');
                break;
        }
    };

    return (
        <>
            <Header
                title="ارسال شکایات"
                search={ null }
            >
                <div className="container">
                    <p className={ styles.headerDescription }>لطفا دلیل عدم رضایت و شکایت خود را در این قسمت برای ما بنویسید</p>
                </div>
            </Header>

            <div className={ classNames('container', styles.body) }>
                <SelectBox>
                    <option>test</option>
                    <option>test 1 </option>
                    <option>test 3 </option>
                    <option>test 5</option>
                </SelectBox>
                <textarea
                    value={ textarea }
                    onChange={ ({ target }) => setTextarea(target.value) }
                    className={ classNames('no-outline', styles.textarea) }
                    placeholder="تایپ کنید"
                />
            </div>

            <Keyboard
                open={ true }
                onChange={ onKeyboardChange }
                primaryButtonTitle={{
                    fa : 'ارسال',
                    en : 'send'
                }}
            />
        </>
    );
}