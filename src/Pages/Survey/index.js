import React, {useEffect, useRef, useState} from 'react';
import PollCard from "./PollCard";
import BluePage from "../../Components/BluePage/BluePage";
import classNames from 'classnames';
import styles from './index.module.scss';
import Button from "../../Components/Button/Button";
import AngleLeftIcon from "../../Components/Icons/AngleLeft";
import AngleRightIcon from "../../Components/Icons/AngleRight";

function questionsOrder(activeIndex, questions){
    const result = [];

    for(let i = activeIndex; i < questions.length; i ++){
        result[i - activeIndex] = {
            ...questions[i],
            index : i
        };
    }
    for(let i = 0; i < activeIndex; i ++){
        result[i + activeIndex] = {
            ...questions[i],
            index : i
        };
    }

    return result;
}

export default function Index({ history }){
    const cardsRef = useRef();
    const [mounted, setMounted] = useState(false);
    const [activeQuestion, setActiveQuestion] = useState(0);
    const [questions, setQuestions] = useState([
        {
            question : "یک . نحوه انجام نمونه‌گیری توسط پرستاران ما را چگونه ارزیابی میکنید؟",
            value : null
        },
        {
            question : "دو . نحوه انجام نمونه‌گیری میکنید؟",
            value : null
        },
        {
            question : "سه . نحوه انجام نمونه‌گیری توسط پرستاران ما را چگونه ارزیابی میکنید؟نحوه انجام نمونه‌گیری توسط پرستاران ما را چگونه ارزیابی میکنید؟",
            value : null
        },
        {
            question : "چهار . نحوه انجام نمونه‌گیری توسط پرنحوه انجام نمونه‌گیری توسط پرستاران ما را چگونه ارزیابی میکنید؟نحوه انجام نمونه‌گیری توسط پرستاران ما را چگونه ارزیابی میکنید؟ستاران ما را چگونه ارزیابی میکنید؟",
            value : null
        }
    ]);

    useEffect(() => {
        if(cardsRef.current){
            let maxHeight = -Infinity;
            [...cardsRef.current.children].forEach(card => {
                maxHeight = Math.max(maxHeight, card.getBoundingClientRect().height);
            });
            cardsRef.current.style.height = maxHeight + 58 + 'px';

            setMounted(true);
        }
    }, []);

    const onNextClick = () => setActiveQuestion(activeQuestion => (activeQuestion + 1) % questions.length);
    const onPreviousClick = () => setActiveQuestion(activeQuestion => (activeQuestion + questions.length - 1) % questions.length);
    const onPollChange = number => ({ target }) => {
        setQuestions(questions => questions.map((question, index) => index === number ? ({
            ...question,
            value : +target.getAttribute('data-value')
        }) : question));
    };
    const onSubmit = () => history.push('/survey/done');

    return (
        <BluePage>
            <div className="container">
                <h1 className={ styles.title }>نظرسنجی و ثبت شکایات</h1>
            </div>

            <div
                ref={ cardsRef }
                className={ classNames('container', styles.cards) }
            >
                {
                    !mounted ? (
                        <>
                            <div/>
                            <div/>
                            <div/>
                        </>
                    ) : null
                }
                {
                    questionsOrder(activeQuestion, questions).map(({ index, ...question }) => (
                        <PollCard
                            key={ index }
                            { ...question }
                            number={ index + 1 }
                            onChange={ onPollChange(index) }
                        />
                    ))
                }
            </div>

            <div className={ classNames('container', styles.progressContainer) }>
                <div className={ styles.progress }>
                    <span style={{ width : ((activeQuestion + 1) * 100 / questions.length) + '%' }}/>
                </div>
                <span className={ styles.progressNumber }>{ questions.length } / { activeQuestion + 1 }</span>
            </div>

            <div className={ styles.footer }>
                <div onClick={ onNextClick } className={ styles.next }>
                    <AngleRightIcon/>
                </div>
                <Button onClick={ onSubmit } color="success">ارسال نظر</Button>
                <div onClick={ onPreviousClick } className={ styles.previous }>
                    <AngleLeftIcon/>
                </div>
            </div>
        </BluePage>
    );
}