import React, {useState} from 'react';
import classNames from 'classnames';
import styles from './index.module.scss';
import PropTypes from 'prop-types';

const buttons = ['ضعیف', 'متوسط', 'خوب', 'عالی'];

export default function PollCard({ className, question, number, value, onChange, ...props }){
    return (
        <div
            className={ classNames(className, styles.container) }
        >
            <span className={ styles.number }>{ number }</span>
            <div className={ styles.question }>{ question }</div>
            <div className={ styles.buttons }>
                {
                    [...buttons].reverse().map((button, index) => {
                        const key = buttons.length - 1 - index;

                        return (
                            <button
                                key={ key }
                                style={{ opacity : value === key || value === null ? 1 : 0.3 }}
                                onClick={ onChange }
                                data-value={ key }
                            >{ button }</button>
                        );
                    })
                }
            </div>
        </div>
    );
}

PollCard.propTypes = {
    number : PropTypes.number.isRequired,
    question : PropTypes.string.isRequired
};