import React from 'react';
import SuccessPage from "../../Components/SucessPage";
import SmsCard from "../../Components/SmsCard/SmsCard";
import ComplaintIcon from 'Assets/img/complaint-icon.svg';

export default function SurveyDone(){
    return (
        <SuccessPage
            description="از اینکه وقت گرانبهای خود را صرف پاسخگویی به سوالات ما کردید، سپاسگزاریم."
            title="از شما ممنونیم!"
        >
            <SmsCard
                href="/survey/complaint"
                style={{ marginTop : 200 }}
                className="padding-50"
                icon={ ComplaintIcon }
                title="ارسال شکایات"
                description="در صورتی از هر کدم از بخش ها شکایتی دارید برای ما ارسال کنید تا مستقیما برای مدیریت ارسال کنیم"
            />
        </SuccessPage>
    )
}