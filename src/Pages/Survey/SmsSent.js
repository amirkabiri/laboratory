import React from 'react';
import SuccessPage from "../../Components/SucessPage";
import styles from './SmsSent.module.scss';

export default function SmsSent(){
    return (
        <SuccessPage
            className={ styles.page }
            description="از اینکه وقت گرانبهای خود را صرف پاسخگویی به سوالات ما کردید، سپاسگزاریم."
            title="از شما ممنونیم!"
        />
    );
}