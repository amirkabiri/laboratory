import React, { useState } from 'react';
import Header from "../../Components/Header/Header";
import GuideCard from "../../Components/GuideCard/GuideCard";
import SmsIcon from 'Assets/img/sms-icon.svg';

const links = [
    'دستور العمل جمع آوری نمونه مدفوع',
    'آزمایش بررسی خون مخفی در مدفوع',
    'آزمایش تستوسترون',
    'آزمایش تحمل لاکتوز',
    'آزمایش گاسترین',
    'آزمایش تست تحریک هورمون رشد',
    'آزمایش کوتیزول',
];

function Home(){
    const [search, setSearch] = useState('');

    const onSearch = value => setSearch(value);

    return (
        <>
            <Header
                title="راهنمای آزمایشات"
                search={{
                    onChange : onSearch
                }}
            >
                <div className="container" style={{ marginTop : 50, transform : 'translateY(30px)' }}>
                    <GuideCard
                        href="/guide/scan"
                        icon={ SmsIcon }
                        title="راهنمای آزمایشات پذیرش شده"
                    />
                </div>
            </Header>

            <div className="container">
                {
                    links.filter(link => new RegExp(search).test(link)).map((title, index) => (
                        <GuideCard
                            key={ index }
                            style={{ marginBottom : 30 }}
                            title={ title }
                            href="/guide/item"
                        />
                    ))
                }
            </div>
        </>
    );
}

export default Home;