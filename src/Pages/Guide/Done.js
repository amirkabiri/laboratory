import React from 'react';
import SmsSentPage from 'Components/SmsSentPage/SmsSentPage';

function Done(){
    const phoneNumber = '09123456789';

    return (
        <SmsSentPage
            title="لینک راهنما برای شما پیامک شد"
            description={ `نحوه انجام آزمایش تحمیل لاکتوز برای شماره ${ phoneNumber } پیامک شد` }
        />
    );
}

export default Done;