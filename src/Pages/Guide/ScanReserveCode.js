import React from 'react';
import SmsCard from "Components/SmsCard/SmsCard";
import HandDialIcon from 'Assets/img/hand-dial.svg';
import ScanPage from "Components/ScanPage";

export default function ScanReserveCode(){
    return (
        <ScanPage title="بارکد پذیرش خود را اسکن کنید">
            <SmsCard
                className="padding-50"
                icon={ HandDialIcon }
                title="وارد کردن دسترسی شماره"
                description="با وارد کردن شماره موبایل خود لینک اطلاعات آمادگی قبل تست را دریافت کنید"
                href="/guide/manual"
            />
        </ScanPage>
    );
}