import React from 'react';
import SmsCard from 'Components/SmsCard/SmsCard';
import PrintedPage from "../../Components/PrintedPage";

export default function Printed(){
    return (
        <PrintedPage title="اطلاعات بر روی کاغذ چاپ شد">
            <SmsCard
                className="padding-50"
                title="ارسال از طریق پیامک"
                description="با وارد کردن شماره موبایل خود لینک اطلاعات امادگی قبل تست را دریافت کنید"
                href="/guide/phone"
            />
        </PrintedPage>
    );
}