import React, { useRef, useState } from 'react';
import BluePage from "../../Components/BluePage/BluePage";
import NumericKeyboard from "../../Components/NumericKeyboard/NumericKeyboard";
import Input from "../../Components/Input/Input";
import Button from "../../Components/Button/Button";

function Reserve({ history }){
    const [activeInput, setActiveInput] = useState();
    const acceptingNumberRef = useRef();
    const nationalNumberRef = useRef();

    const onInputFocus = ref => () => {
        setActiveInput(ref);
    };

    const onSubmit = () => {
        history.push('/guide/printed');
    };

    return (
        <BluePage>
            <div className="container">
                <div className="header-title">شماره پذیرش و کدملی خود را وارد کنید</div>

                <Input
                    onFocus={ onInputFocus(acceptingNumberRef) }
                    ref={ acceptingNumberRef }
                    placeholder="شماره پذیرش"
                    style={{ marginTop : 70, marginBottom : 40 }}
                    autoFocus={ true }
                />
                <Input
                    onFocus={ onInputFocus(nationalNumberRef) }
                    ref={ nationalNumberRef }
                    placeholder="کدملی"
                />

                <div className="container flex justify-content-center" style={{ marginTop : 70 }}>
                    <NumericKeyboard
                        inputRef={ activeInput }
                    />
                </div>

                <div className="flex justify-content-center" style={{ marginTop : 70 }}>
                    <Button onClick={ onSubmit } color="success">تایید</Button>
                </div>
            </div>
        </BluePage>
    );
}

export default Reserve;