import React from 'react';
import BluePage from "../../Components/BluePage/BluePage";
import SmsCard from 'Components/SmsCard/SmsCard';
import styles from './ShowItem.module.css';
import PrintCard from "../../Components/PrintCard/PrintCard";

export default function Printed(){
    return (
        <BluePage>
            <div className="container flex flex-column align-items-center">
                <div className={ styles.title }>آزمایش تحمل لاکتوز</div>

                <p className={ styles.description }>آمادگی قبل از تست هر آزمایشی مخصوص به همان آزمایش می باشد که برای نتیجه دقیق میبایست موارد ذکر شده را حتما رعایت کرد</p>

                <SmsCard
                    className={ styles.smsCard }
                    title="ارسال پیامک"
                    description="با وارد کردن شماره موبایل خود لینک اطلاعات آمادگی قبل از تست را دریافت کنید"
                    href="/guide/phone"
                />
                <PrintCard
                    className={ styles.printCard }
                    title="چاپ"
                    description="با انتخاب دکمه چاپ اطلاعات مربوط به آمادگی قبل تست را بر روی کاغذ دریافت کنید"
                    href="/guide/printed"
                />
            </div>
        </BluePage>
    );
}