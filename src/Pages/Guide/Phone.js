import React from 'react';
import EnterPhonePage from "Components/EnterPhonePage";

function Phone({ history }){
    const onSubmit = () => history.push('/guide/done');

    return (
        <EnterPhonePage onSubmit={ onSubmit }/>
    )
}

export default Phone;