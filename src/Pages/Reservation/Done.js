import React from 'react';
import SmsSentPage from "Components/SmsSentPage/SmsSentPage";

function Done(){
    const phoneNumber = '09123456789';

    return (
        <SmsSentPage
            title="شماره شما برای یادآوری ثبت شد"
            description={ `15 دقیقه مانده به نوبت، پیامک اطلاع رسانی برای شماره ${ phoneNumber } ارسال خواهد شد` }
        />
    );
}

export default Done;