import React from 'react';
import BluePage from "../../Components/BluePage/BluePage";
import ReservationResultIcon from 'Assets/img/reservation-result-icon.svg';
import SmsCard from "../../Components/SmsCard/SmsCard";
import ArrowDownIcon from 'Assets/img/arrow-down.svg';
import styles from './Result.module.css';
import classNames from 'classnames';

function Result(){
    return (
        <BluePage>
            <div className={ classNames('container flex flex-column align-items-center') }>
                <img src={ ReservationResultIcon } alt="page logo"/>
                
                <div className="header-title" style={{ margin : '80px 0' }}>رسید نوبت خود را بردارید</div>

                <SmsCard
                    style={{ padding: 80 }}
                    href="/reservation/phone"
                    title="دریافت پیامک یادآوری"
                    description="شما میتوانید با وارد کردن شماره تماس خود 15 دقیق قبل از رسیدن نوبت پیامک اطلاع رسانی دریافت کنید"
                />

                <img className={ styles.arrow } src={ ArrowDownIcon } alt="arrow down icon"/>
            </div>
        </BluePage>
    );
}

export default Result;