import React from 'react';
import ReservationInfoPage from "Components/ReservationInfoPage/ReservationInfoPage";

function Reserve({ history }){
    return (
        <ReservationInfoPage
            onSubmit={ () => history.push('/reservation/result') }
        />
    )
}

export default Reserve;