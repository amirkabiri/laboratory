import React from 'react';
import EnterPhonePage from "../../Components/EnterPhonePage";

function Phone({ history }){
    return (
        <EnterPhonePage
            onSubmit={ () => history.push('/reservation/done') }
        />
    );
}

export default Phone;