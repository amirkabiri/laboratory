import React from 'react';
import Header from 'Components/Header/Header';
import HomeCard from 'Components/HomeCard/HomeCard';
import ReservationAlert from 'Components/ReservationAlert/ReservationAlert';

import QueueIcon from 'Assets/img/home-icons/queue.svg';
import ExperimentResultIcon from 'Assets/img/home-icons/experiment-result.svg';
import ExampleIcon from 'Assets/img/example-icon.svg';
import CashBoxIcon from 'Assets/img/cash-box-icon.svg';

function Home(){
    return (
        <>
            <Header
                title="نوبت دهی"
                search={ null }
            />
            <div className="container">
                <ReservationAlert/>
            </div>
            <div className="container flex justify-content-between">
                <HomeCard
                    title="جواب دهی"
                    icon={ ExperimentResultIcon }
                />
                <HomeCard
                    title="پذیرش"
                    icon={ QueueIcon }
                    href="/reservation"
                />
            </div>
            <div className="container flex justify-content-between">
                <HomeCard
                    title="صندوق"
                    icon={ CashBoxIcon }
                />
                <HomeCard
                    title="نمونه گیری"
                    icon={ ExampleIcon }
                    href="/reservation"
                />
            </div>
        </>
    );
}

export default Home;