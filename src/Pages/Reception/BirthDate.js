import React, {useRef, useState} from 'react';
import styles from './BirthDate.module.scss';
import BluePage from "../../Components/BluePage/BluePage";
import Button from "../../Components/Button/Button";
import NumericKeyboard from "../../Components/NumericKeyboard/NumericKeyboard";
import Input from "../../Components/Input/Input";

export default function BirthDate({ history }) {
    const [activeRef, setActiveRef] = useState('year');
    const [invalid, setInvalid] = useState({
        day : false,
        month : false,
        year : false
    });
    const refs = {
        day : useRef(),
        month : useRef(),
        year : useRef()
    };

    const onInputFocus = refName => () => setActiveRef(refName);
    const onKeyboardChange = e => {
        const ref = refs[activeRef];
        const input = ref.current;
        if(!ref || !input) return;

        const newValue = String(e.type === 'number' ? input.value + e.value : input.value);

        if(e.type === 'number' &&
            (input.maxLength && input.maxLength >= newValue.length) &&
            (input.max && parseInt(input.max) >= parseInt(newValue))
        ){
            input.value += e.value;
        }else if(e.type === 'escape'){
            input.value = input.value.slice(0, input.value.length - 1);
        }
    };
    const onSubmit = () => {
        setInvalid(invalid => Object.fromEntries(Object.keys(invalid).map(key => [key, false])));
        let isValid = true;

        for(const key of Object.keys(refs)){
            const ref = refs[key].current;

            // console.log({
            //     ref,
            //     maxLength : ref.maxLength,
            //     min : ref.min,
            //     max : ref.max,
            //     value : ref.value,
            //
            // });

            if(ref.value.length === 0 ||
                ref.value.length > ref.maxLength ||
                parseInt(ref.value) > parseInt(ref.max) ||
                parseInt(ref.value) < parseInt(ref.min)
            ){
                setInvalid(invalid => ({
                    ...invalid,
                    [key] : true
                }));

                isValid = false;
            }
        }

        if(isValid) history.push('/reception/checkout');
    };

    return (
        <BluePage>
            <div className="container flex flex-column align-items-center">
                <h1 className={ styles.title }>تاریخ تولد خود را وارد کنید</h1>
                <div className={ styles.inputs }>
                    <Input
                        ref={ refs.day }
                        className={ styles.day }
                        placeholder="روز"
                        onFocus={ onInputFocus('day') }
                        max={ 31 }
                        maxLength={ 2 }
                        min={ 1 }
                        style={{
                            color : invalid.day ? '#F0423B' : '#333333',
                            border : (invalid.day ? '5px solid #F0423B' : 0)
                        }}
                    />
                    <Input
                        ref={ refs.month }
                        className={ styles.month }
                        placeholder="ماه"
                        onFocus={ onInputFocus('month') }
                        max={ 12 }
                        maxLength={ 2 }
                        min={ 1 }
                        style={{
                            color : invalid.month ? '#F0423B' : '#333333',
                            border : (invalid.month ? '5px solid #F0423B' : 0)
                        }}
                    />
                    <Input
                        ref={ refs.year }
                        className={ styles.year }
                        placeholder="سال"
                        onFocus={ onInputFocus('year') }
                        max={ 9999 }
                        maxLength={ 4 }
                        min={ 1399 }
                        style={{
                            color : (invalid.year ? '#F0423B' : '#333333'),
                            border : (invalid.year ? '5px solid #F0423B' : 0)
                        }}
                    />
                </div>
                <NumericKeyboard
                    className={ styles.keyboard }
                    onChange={ onKeyboardChange }
                />
                <Button
                    onClick={ onSubmit }
                    color="success">تایید</Button>
            </div>
        </BluePage>
    );
}