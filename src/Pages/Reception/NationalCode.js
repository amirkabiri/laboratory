import React, {useRef, useState} from 'react';
import BluePage from "../../Components/BluePage/BluePage";
import Input from "../../Components/Input/Input";
import NumericKeyboard from "../../Components/NumericKeyboard/NumericKeyboard";
import Button from "../../Components/Button/Button";
import validateNationalCode from '../../Shared/validateNationalCode';

export default function NationalCode({ history }) {
    const nationalCodeRef = useRef();
    const [invalid, setInvalid] = useState(false);

    const onSubmit = () => {
        const nationalCode = nationalCodeRef.current.value;

        if(!validateNationalCode(nationalCode)){
            return setInvalid(true);
        }

        history.push('/reception/name');
    };

    return (
        <BluePage>
            <div className="container flex flex-column align-items-center">
                <h1
                    style={{ color : 'white', fontSize : 60 }}
                >کد ملی خود را وارد کنید</h1>
                <Input
                    invalid={ invalid }
                    style={{ margin: '70px 0' }}
                    ref={ nationalCodeRef }
                />
                <NumericKeyboard
                    inputRef={ nationalCodeRef }
                />
                <Button
                    onClick={ onSubmit }
                    color="success"
                    style={{ marginTop : 70 }}
                >تایید</Button>
            </div>
        </BluePage>
    );
}