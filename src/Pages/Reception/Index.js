import React, { useState } from 'react';
import Header from "../../Components/Header/Header";
import GuideCard from "../../Components/GuideCard/GuideCard";

export default function Index() {
    const [search, setSearch] = useState('');
    const [experiments, setExperiments] = useState([
        { name : 'آزمایش چکاپ' },
        { name : 'آزمایش ازدواج' },
        { name : 'آزمایش بارداری' },
        { name : 'آزمایش کم خونی' },
        { name : 'آزمایش گاسترین' },
        { name : 'آزمایش تست تحریک هورمون رشد' },
        { name : 'آزمایش کورتیزول' },
        { name : 'آزمایش آهن و TIBC' },
        { name : 'آزمایش شناخت زودرس سرطان دهانه رحم' },
    ]);

    const onSearch = search => setSearch(search);

    return (
        <>
            <Header
                title="پذیرش آزمایشات مشخص"
                search={{
                    onChange : onSearch
                }}
            />
            <div className="container">
                {
                    experiments
                        .filter(experiment => new RegExp(search).test(experiment.name))
                        .map((experiment, index) => (
                        <GuideCard
                            title={ experiment.name }
                            key={ index }
                            href="/reception/national-code"
                            style={{ marginBottom : 20 }}
                        />
                    ))
                }
            </div>
        </>
    );
}

Index.defaultProps = {};
Index.propTypes = {};