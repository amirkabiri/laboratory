import React, {useState} from 'react';
import BluePage from "../../Components/BluePage/BluePage";
import Keyboard from "../../Components/Keyboard/Keyboard";
import Input from "../../Components/Input/Input";
import styles from './Name.module.scss';
import classNames from 'classnames';

export default function Name({ history }) {
    const [state, setState] = useState({
        firstName : '',
        lastName : '',
        sex : 'female'
    });
    const [activeInput, setActiveInput] = useState('firstName');

    const onSexClick = sex => () => setState(state => ({
        ...state,
        sex
    }));
    const onKeyboardChange = e => {
        switch (e.type) {
            case 'char':
                setState(state => ({
                    ...state,
                    [activeInput] : state[activeInput] + e.value
                }));
                break;

            case 'escape':
                setState(state => ({
                    ...state,
                    [activeInput] : state[activeInput].slice(0, state[activeInput].length - 1)
                }));
                break;

            case 'search':
                history.push('/reception/static-phone');
                break;
        }
    };
    const onInputFocus = input => () => setActiveInput(input);

    return (
        <BluePage>
            <div className="container">
                <div className={ styles.input }>
                    <span>نام</span>
                    <Input
                        value={ state.firstName }
                        onFocus={ onInputFocus('firstName') }
                        onChange={ () => null }
                    />
                </div>
                <div className={ styles.input } style={{ marginTop : 80 }}>
                    <span>نام خانوادگی</span>
                    <Input
                        value={ state.lastName }
                        onFocus={ onInputFocus('lastName') }
                        onChange={ () => null }
                    />
                </div>
                <div className={ styles.gender }>
                    <label
                        className={ classNames(styles.radio, { [styles.active] : state.sex === 'female' }) }
                        onClick={ onSexClick('female') }
                    >
                        <span/>
                        زن
                    </label>
                    <label
                        className={ classNames(styles.radio, { [styles.active] : state.sex === 'male' }) }
                        onClick={ onSexClick('male') }
                    >
                        <span/>
                        مرد
                    </label>
                </div>
            </div>
            <Keyboard
                onChange={ onKeyboardChange }
                primaryButtonTitle={{
                    fa : 'ثبت',
                    en : 'submit'
                }}
            />
        </BluePage>
    );
}

Name.defaultProps = {};
Name.propTypes = {};