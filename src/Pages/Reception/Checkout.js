import React from 'react';
import CheckoutPage from "../../Components/CheckoutPage";

export default function Checkout({className, ...props}) {
    return (
        <CheckoutPage
            cost="2,380,000"
        >
            <h5
                style={{
                    color : 'white',
                    fontSize : 30,
                    position : 'relative',
                    top : -40
                }}
            >آزمایش چکاپ</h5>
        </CheckoutPage>
    );
}