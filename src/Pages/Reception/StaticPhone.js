import React from 'react';
import EnterPhonePage from "../../Components/EnterPhonePage";

export default function StaticPhone({ history }) {
    return (
        <EnterPhonePage
            title="تلفن ثابت خود را وارد کنید"
            placeholder="شماره ثابت"
            validation="static-phone"
            onSubmit={ () => history.push('/reception/phone') }
        />
    );
}