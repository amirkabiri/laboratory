import React from 'react';
import CheckoutSuccessPage from "../../Components/CheckoutSuccessPage";

export default function Done() {
    return (
        <CheckoutSuccessPage
            description="اطلاعات چاپ شده خود را از محفظه خصوص چاپ فیش بردارید"
            title="پذیرش شما انجام شد"
        >
            <h5
                style={{
                    color : 'white',
                    fontSize : 30,
                    marginBottom : 50
                }}
            >آزمایش چکاپ</h5>
        </CheckoutSuccessPage>
    );
}