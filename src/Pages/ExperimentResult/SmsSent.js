import React from 'react';
import SmsSentPage from "../../Components/SmsSentPage/SmsSentPage";

export default function SmsSent(){
    const phoneNumber = '09146878528';

    return (
        <SmsSentPage
            title="پیامک ارسال شد"
            description={ `جواب آزمایش به شماره موبایل ${ phoneNumber } پیامک شد.` }
        />
    );
}