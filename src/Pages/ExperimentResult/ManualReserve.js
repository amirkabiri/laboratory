import React from 'react';
import ReservationInfoPage from "../../Components/ReservationInfoPage/ReservationInfoPage";

export default function ManualReserve({ history }){
    return (
        <ReservationInfoPage
            onSubmit={ () => history.push('/experiment-result/checkout') }
        />
    );
}