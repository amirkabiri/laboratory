import React from 'react';
import BluePage from "../../Components/BluePage/BluePage";
import ErrorIcon from "../../Components/Icons/Error";
import PaperExclamationIcon from "../../Components/Icons/PaperExclamation";
import Button from "../../Components/Button/Button";
import styles from './NotReady.module.scss';

export default function NotReady(){
    return (
        <BluePage>
            <div className="flex flex-column align-items-center justify-content-center">
                <ErrorIcon className={ styles.errorIcon }/>

                <div className="container flex flex-column align-items-center justify-content-center">
                    <h1 className={ styles.title }>جواب آزمایش شما آماده نیست</h1>

                    <p className={ styles.description }>حداقل زمان لازم برای آماده‌سازی جواب آزمایش شما ۵ روز می‌باشد</p>
                </div>

                <PaperExclamationIcon className={ styles.paperIcon }/>

                <Button className={ styles.button } color="success">فهمیدم</Button>
            </div>
        </BluePage>
    );
}