import React from 'react';
import PrintedPage from "Components/PrintedPage";
import SmsCard from "Components/SmsCard/SmsCard";

export default function Printed(){
    return (
        <PrintedPage
            title="اطلاعات بر روی کاغذ چاپ شد"
        >
            <SmsCard
                className="padding-50"
                title="ارسال از طریق پیامک"
                description="با وارد کردن شماره موبایل خود لینک اطلاعات امادگی قبل تست را دریافت کنید"
                href="/experiment-result/phone"
            />
        </PrintedPage>
    );
}