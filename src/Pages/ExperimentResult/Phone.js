import React from 'react';
import EnterPhonePage from "../../Components/EnterPhonePage";

export default function Phone({ history }){
    const onSubmit = () => history.push('/experiment-result/sms-sent');

    return (
        <EnterPhonePage onSubmit={ onSubmit }/>
    );
}