import React, { useEffect } from 'react';
import CheckoutPage from "../../Components/CheckoutPage";

export default function Checkout({ history }){
    useEffect(() => {
        const timeout = setTimeout(() => {
            history.push('/experiment-result/result');
        }, 3000);
        return () => clearTimeout(timeout);
    }, []);

    return (
        <CheckoutPage
            title="هزینه آزمایش را تسویه کنید"
            cost="2,380,000"
        />
    );
}