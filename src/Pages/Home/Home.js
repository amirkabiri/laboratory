import React, { useState } from 'react';
import Header from 'Components/Header/Header';
import CallIcon from 'Assets/img/call-icon.svg';
import HomeCard from 'Components/HomeCard/HomeCard';
import styles from './Home.module.css';
import classNames from 'classnames';

import QueueIcon from 'Assets/img/home-icons/queue.svg';
import ExperimentTipIcon from 'Assets/img/home-icons/experiment-tip.svg';
import ExperimentResultIcon from 'Assets/img/home-icons/experiment-result.svg';
import AcceptingExperimentIcon from 'Assets/img/home-icons/accepting-experiments.svg';
import PayIcon from 'Assets/img/home-icons/pay.svg';
import VoteIcon from 'Assets/img/home-icons/vote.svg';

function Home(){
    const [search, setSearch] = useState('');
    const [items, setItems] = useState([
        {
            title : "نوبت دهی",
            icon : QueueIcon,
            href : "/reservation",
        }, {

            title : "راهنمای آزمایشات",
            icon : ExperimentTipIcon,
            href : "/guide",
        }, {
            title : "جواب آزمایش",
            icon : ExperimentResultIcon,
            href : "/experiment-result/scan",
        }, {
            title : "پذیرش آزمایشات",
            icon : AcceptingExperimentIcon,
            href : "/reception",
        }, {
            title : "تسویه حساب",
            icon : PayIcon,
            href : "/checkout/scan",
        }, {
            title : "نظرسنجی",
            icon : VoteIcon,
            href : "/survey",
        }
    ]);

    const onSearch = search => setSearch(search);

    return (
        <>
            <Header
                title="به آزمایشگاه پرنیا خوش آمدید"
                button={{
                    title : 'اطلاعات تماس',
                    icon : CallIcon,
                    href : '/contact'
                }}
                containerClass={ styles.container }
                search={{
                    onChange : onSearch
                }}
            />
            <div className={ classNames('container flex justify-content-between', styles.container) }>
                <div className={ styles.cards }>
                    {
                        items.filter(item => new RegExp(search).test(item.title)).map(item => (
                            <HomeCard
                                key={ item.title }
                                { ...item }
                            />
                        ))
                    }
                </div>
            </div>
        </>
    );
}

export default Home;