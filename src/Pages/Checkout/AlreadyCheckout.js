import React from 'react';
import styles from './AlreadyCheckout.module.scss';
import SuccessIcon from 'Assets/img/success-icon.svg';
import BluePage from "../../Components/BluePage/BluePage";
import QrcodeIcon from 'Assets/img/qrcode-icon.svg';

export default function AlreadyCheckout({className, ...props}) {
    return (
        <BluePage>
            <div className="container flex flex-column align-items-center">
                <img className={ styles.successIcon } src={ SuccessIcon } alt="success icon"/>
                <h1 className={ styles.title }>هزینه این رسید قبلا تسویه شده است</h1>
                <img className={ styles.qrcodeIcon } src={ QrcodeIcon } alt="qrcode icon"/>
                <div className={ styles.cost }>
                    <span>پرداخت شده</span>
                    <div>
                        <span>2,380,000</span>
                        ریال
                    </div>
                </div>
            </div>
        </BluePage>
    );
}

AlreadyCheckout.defaultProps = {};
AlreadyCheckout.propTypes = {};