import React from 'react';
import ScanPage from "../../Components/ScanPage";
import SmsCard from "../../Components/SmsCard/SmsCard";
import SmsIcon from 'Assets/img/home-icons/queue.svg';

function Scan(){
    return (
        <ScanPage
            title="بارکد پذیرش خود را اسکن کنید"
        >
            <SmsCard
                href="/checkout/reserve"
                className="padding-50"
                title="مشاهده با شماره پذیرش"
                description="با وارد کردن شماره موبایل خود لینک اطلاعات آمادگی قبل تست را دریافت کنید"
                icon={ SmsIcon }
            />
        </ScanPage>
    );
}

export default Scan;