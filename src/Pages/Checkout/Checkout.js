import React from 'react';
import CheckoutPage from "../../Components/CheckoutPage";

export default function Checkout({className, ...props}) {
    return (
        <CheckoutPage
            title="پرداخت کنید ..."
            cost="2,380,000"
        />
    );
}

Checkout.defaultProps = {};
Checkout.propTypes = {};