import React from 'react';
import ReservationInfoPage from "../../Components/ReservationInfoPage/ReservationInfoPage";

export default function Reserve({ history }) {
    return (
        <ReservationInfoPage
            onSubmit={ () => history.push('/checkout/checkout') }
        />
    );
}

Reserve.defaultProps = {};
Reserve.propTypes = {};