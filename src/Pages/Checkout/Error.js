import React from 'react';
import CheckoutErrorPage from "../../Components/CheckoutErrorPage";
import Button from "../../Components/Button/Button";

export default function Error() {
    return (
        <CheckoutErrorPage
            description="اطلاعات چاپ شده خود را از محفظه مخصوص چاپ فیش بردارید"
            title="پرداخت شما انجام نشد"
        >
            <Button color="success" style={{ marginTop : 50 }}>تلاش مجدد</Button>
        </CheckoutErrorPage>
    );
}