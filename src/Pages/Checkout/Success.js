import React from 'react';
import CheckoutSuccessPage from "../../Components/CheckoutSuccessPage";

export default function Success() {
    return (
        <CheckoutSuccessPage
            description="اطلاعات چاپ شده خود را از محفظه مخصوص چاپ فیش بردارید"
            title="پرداخت شما با موفقیت انجام شد"
        />
    );
}

Success.defaultProps = {};
Success.propTypes = {};