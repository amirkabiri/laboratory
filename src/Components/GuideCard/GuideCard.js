import React from 'react';
import styles from './GuideCard.module.css';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import ArrowLeftIcon from 'Assets/img/arrow-left-icon.svg';
import { Link } from 'react-router-dom';

function GuideCard({ className, icon, title, href, ...props }){
    return (
        <Link
            to={ href }
            className={ classNames('card', className, styles.container) }
            { ...props }
        >
            {
                icon ? (
                    <img className={ styles.icon } src={ icon } alt="guide card icon"/>
                ) : null
            }
            <div className={ styles.body }>
                <h3 className={ styles.title }>{ title }</h3>
            </div>

            <img className={ styles.arrowIcon } src={ ArrowLeftIcon } alt="arrow left icon"/>
        </Link>
    );
}

GuideCard.defaultProps = {
    href : '/guide'
};
GuideCard.propTypes = {
    icon : PropTypes.elementType,
    title : PropTypes.string,
    className : PropTypes.string,
    href : PropTypes.string
};

export default GuideCard;