import React from 'react';
import styles from './NumericKeyboard.module.css';
import InputEscapeIcon from 'Assets/img/input-escape-icon.svg';
import classNames from 'classnames';

function NumericKeyboard({ className, inputRef, onChange, ...props }){
    const checkInputIsAvailable = () => inputRef && inputRef.current;
    const onNumericButtonClick = ({ target }) => {
        const value = target.innerText.trim();

        if(onChange) onChange({
            type : 'number',
            value
        });

        if(checkInputIsAvailable()){
            inputRef.current.value += value;
        }
    };
    const onEscapeButtonClick = () => {
        if(onChange) onChange({
            type : 'escape'
        });

        if(checkInputIsAvailable()){
            const value = inputRef.current.value;
            inputRef.current.value = value.substr(0, value.length - 1);
        }
    };

    const numericButtonClassName = classNames(styles.numericButton, styles.ripple);

    return (
        <div className={ classNames(styles.container, className) } { ...props }>
            <div className={ styles.row }>
                <button onClick={ onNumericButtonClick } className={ numericButtonClassName }>3</button>
                <button onClick={ onNumericButtonClick } className={ numericButtonClassName }>2</button>
                <button onClick={ onNumericButtonClick } className={ numericButtonClassName }>1</button>
            </div>
            <div className={ styles.row }>
                <button onClick={ onNumericButtonClick } className={ numericButtonClassName }>6</button>
                <button onClick={ onNumericButtonClick } className={ numericButtonClassName }>5</button>
                <button onClick={ onNumericButtonClick } className={ numericButtonClassName }>4</button>
            </div>
            <div className={ styles.row }>
                <button onClick={ onNumericButtonClick } className={ numericButtonClassName }>9</button>
                <button onClick={ onNumericButtonClick } className={ numericButtonClassName }>8</button>
                <button onClick={ onNumericButtonClick } className={ numericButtonClassName }>7</button>
            </div>
            <div className={ styles.lastRow }>
                <button onClick={ onEscapeButtonClick } className={ classNames(styles.escapeButton, styles.ripple) }>
                    <img src={ InputEscapeIcon } alt=""/>
                </button>
                <button onClick={ onNumericButtonClick } className={ numericButtonClassName }>0</button>
            </div>
        </div>
    );
}

export default NumericKeyboard;