import React, { forwardRef } from 'react';
import SearchIcon from '../../Assets/img/search-icon.svg';
import styles from './SearchInput.module.css';
import PropTypes from "prop-types";
import classNames from 'classnames';

const SearchInput = forwardRef(({ placeholder, className, style, ...props }, ref) => (
    <div className={ classNames(styles.container, className) } style={ style }>
        <img src={ SearchIcon } className={ styles.icon }/>
        <input
            ref={ ref }
            className={ styles.input }
            placeholder={ placeholder }
            { ...props }
        />
    </div>
));

SearchInput.defaultProps = {
    placeholder: 'اینجا جستجو کنید',
    style: {}
};
SearchInput.propTypes = {
    placeholder: PropTypes.string,
    style: PropTypes.object
};

export default SearchInput;