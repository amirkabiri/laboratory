import React from 'react';

export default function Escape(color){
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="104.8" height="81.382" viewBox="0 0 104.8 81.382">
            <g id="Group_2277" data-name="Group 2277" transform="translate(2 2)">
                <path id="Path_2329" data-name="Path 2329" d="M48.552,0h61.509A7.937,7.937,0,0,1,118,7.937V69.445a7.937,7.937,0,0,1-7.937,7.937H48.552L17.2,38.912Z" transform="translate(-17.197)" fill="none"
                      stroke={ color } strokeLinecap="round" strokeLinejoin="round" strokeWidth="4"/>
                <g id="Group_944" data-name="Group 944" transform="translate(60.45 21.379) rotate(45)">
                    <line id="Line_71" data-name="Line 71" y2="26" transform="translate(13 0)" fill="none"
                          stroke={ color } strokeLinecap="round" strokeWidth="4"/>
                    <line id="Line_72" data-name="Line 72" x2="26" transform="translate(0 13)" fill="none"
                          stroke={ color } strokeLinecap="round" strokeWidth="4"/>
                </g>
            </g>
        </svg>
    );
}

Escape.defaultProps = {
    color : '#fff'
};