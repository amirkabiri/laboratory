import React from 'react';

export default function AngleLeft(props){
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="46.365" height="46.365" viewBox="0 0 46.365 46.365" { ...props }>
            <g id="Group_2079" data-name="Group 2079" transform="translate(6.364 3.182)">
                <path id="Path_3836" data-name="Path 3836" d="M28.285,28.285H0V0" transform="translate(20.001 0) rotate(45)" fill="none" stroke="#fff" strokeWidth="9"/>
            </g>
        </svg>
    );
}