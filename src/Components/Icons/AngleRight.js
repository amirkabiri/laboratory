import React from 'react';

export default function AngleRight(props){
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="46.365" height="46.365" viewBox="0 0 46.365 46.365" { ...props }>
            <path id="Path_3836" data-name="Path 3836" d="M28.285,0H0V28.285" transform="translate(40.001 23.183) rotate(135)" fill="none" stroke="#fff" strokeWidth="9"/>
        </svg>
    );
}