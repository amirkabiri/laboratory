import React from 'react';

export default function Error({ ...props }){
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="170" height="170" viewBox="0 0 170 170" { ...props }>
            <defs>
                <radialGradient id="radial-gradient" cx="0.5" cy="0.5" r="0.724" gradientTransform="translate(-0.073 0.915) rotate(-80.934)" gradientUnits="objectBoundingBox">
                    <stop offset="0" stopColor="#fd6185"/>
                    <stop offset="1" stopColor="#ff8da7"/>
                </radialGradient>
            </defs>
            <g id="Group_2670" data-name="Group 2670" transform="translate(-455 -365)">
                <g id="Group_2127" data-name="Group 2127" transform="translate(146.318 -54.949)">
                    <circle id="Ellipse_111" data-name="Ellipse 111" cx="85" cy="85" r="85" transform="translate(308.682 419.949)" fill="url(#radial-gradient)"/>
                    <line id="Line_108" data-name="Line 108" y2="64" transform="translate(416.809 484.822) rotate(45)" fill="none" stroke="#fff" strokeLinecap="round" strokeWidth="13"/>
                </g>
                <line id="Line_109" data-name="Line 109" x2="64" transform="translate(519.287 428.458) rotate(45)" fill="none" stroke="#fff" strokeLinecap="round" strokeWidth="13"/>
            </g>
        </svg>
    );
};

