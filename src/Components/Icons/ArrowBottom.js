import React from 'react';

export default function ArrowBottom(props){
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="117.174" height="162.501" viewBox="0 0 117.174 162.501" { ...props }>
            <defs>
                <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                    <stop offset="0" stopColor="#3dafef"/>
                    <stop offset="1" stopColor="#eaf6ff"/>
                </linearGradient>
            </defs>
            <g id="Group_2206" data-name="Group 2206" transform="translate(-0.002 -123.182)">
                <g id="Group_2205" data-name="Group 2205">
                    <path id="Subtraction_6" data-name="Subtraction 6" d="M14896.59-3244.317a9.024,9.024,0,0,1-3.716-.79,7.649,7.649,0,0,1-2.986-2.372l-50.221-66.959a8.4,8.4,0,0,1-.275-9.644,8.336,8.336,0,0,1,6.962-3.747,8.308,8.308,0,0,1,2.044.256l23.082,5.765v-85.009h50.217v85.009l23.081-5.765a8.25,8.25,0,0,1,2.038-.256,8.353,8.353,0,0,1,6.972,3.747,8.391,8.391,0,0,1-.279,9.644l-50.217,66.959a7.635,7.635,0,0,1-2.986,2.372A9.016,9.016,0,0,1,14896.59-3244.317Z" transform="translate(-14838 3530)" fill="url(#linear-gradient)"/>
                </g>
            </g>
        </svg>
    );
}