import React from 'react';

export default function Checkout(props){
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="540.123" height="532.852" viewBox="0 0 540.123 532.852" { ...props }>
            <defs>
                <filter id="Union_5" x="53.002" y="0" width="487.121" height="532.852" filterUnits="userSpaceOnUse">
                    <feOffset dx="20" dy="53"/>
                    <feGaussianBlur stdDeviation="18" result="blur"/>
                    <feFlood floodColor="#204ba0" floodOpacity="0.231"/>
                    <feComposite operator="in" in2="blur"/>
                    <feComposite in="SourceGraphic"/>
                </filter>
            </defs>
            <g id="Group_2294" data-name="Group 2294" transform="translate(-306 -1098)">
                <g transform="matrix(1, 0, 0, 1, 306, 1098)" filter="url(#Union_5)">
                    <path id="Union_5-2" data-name="Union 5" d="M16210.126,3475.852a4.127,4.127,0,0,1-4.124-4.129V3055.129a4.127,4.127,0,0,1,4.124-4.129h245.157a4.128,4.128,0,0,1,4.129,4.129v67.1h59.01v0h28.928v0h20.107a17.666,17.666,0,0,1,17.666,17.665v202.95a17.666,17.666,0,0,1-17.666,17.665h-108.045v111.217a4.128,4.128,0,0,1-4.129,4.129Z" transform="translate(-16119 -3050)" fill="#fff"/>
                </g>
                <g id="Group_2293" data-name="Group 2293">
                    <circle id="Ellipse_115" data-name="Ellipse 115" cx="234" cy="234" r="234" transform="translate(306 1123)" fill="#fff" opacity="0.149"/>
                    <g id="pos" transform="translate(374.75 1086)">
                        <path id="Path_3865" data-name="Path 3865" d="M79.09,30.1H191.523a18.048,18.048,0,0,1,18.048,18.048V255.5a18.048,18.048,0,0,1-18.048,18.048H79.09V30.1Z" transform="translate(190.572 50.954)" fill="#abb2c0"/>
                        <path id="Path_3879" data-name="Path 3879" d="M4.128,0H249.284a4.128,4.128,0,0,1,4.128,4.128v416.6a4.128,4.128,0,0,1-4.128,4.128H4.128A4.128,4.128,0,0,1,0,420.724V4.128A4.128,4.128,0,0,1,4.128,0Z" transform="translate(18.25 13)" fill="#fff"/>
                        <path id="Path_3866" data-name="Path 3866" d="M25.968,21.5H215.081v85.1H25.968Z" transform="translate(24.429 26.904)" fill="#d3d7e2"/>
                        <path id="Path_3867" data-name="Path 3867" d="M55.253,51.465h67.135V86.449H55.253Z" transform="translate(117.122 121.75)" fill="#ffd5d5"/>
                        <path id="Path_3868" data-name="Path 3868" d="M55.253,66.75h67.135v34.984H55.253Z" transform="translate(117.122 170.13)" fill="#fff2bc"/>
                        <path id="Path_3869" data-name="Path 3869" d="M55.253,82.034h67.135v99.79H55.253Z" transform="translate(117.122 218.507)" fill="#bdf2d5"/>
                        <g id="Group_2220" data-name="Group 2220" transform="translate(48.981 173.215)">
                            <path id="Path_3870" data-name="Path 3870" d="M25.628,51.465H60.616V86.453H25.628Z" transform="translate(-25.628 -51.465)" fill="#ececf1"/>
                            <path id="Path_3871" data-name="Path 3871" d="M40.27,51.465H75.254V86.453H40.27Z" transform="translate(20.717 -51.465)" fill="#ececf1"/>
                            <path id="Path_3872" data-name="Path 3872" d="M25.628,66.75H60.616v34.984H25.628Z" transform="translate(-25.628 -3.085)" fill="#ececf1"/>
                            <path id="Path_3873" data-name="Path 3873" d="M40.27,66.75H75.254v34.984H40.27Z" transform="translate(20.717 -3.085)" fill="#ececf1"/>
                            <path id="Path_3874" data-name="Path 3874" d="M25.628,82.034H60.616v34.984H25.628Z" transform="translate(-25.628 45.292)" fill="#ececf1"/>
                            <path id="Path_3875" data-name="Path 3875" d="M40.27,82.034H75.254v34.984H40.27Z" transform="translate(20.717 45.292)" fill="#ececf1"/>
                            <path id="Path_3876" data-name="Path 3876" d="M25.628,97.592H60.616v34.984H25.628Z" transform="translate(-25.628 94.537)" fill="#ececf1"/>
                            <path id="Path_3877" data-name="Path 3877" d="M40.27,97.592H75.254v34.984H40.27Z" transform="translate(20.717 94.537)" fill="#ececf1"/>
                        </g>
                        <path id="Path_3878" data-name="Path 3878" d="M93.257,30.1h28.927v243.45H93.257Z" transform="translate(237.413 50.955)" fill="#fff"/>
                    </g>
                </g>
            </g>
        </svg>
    );
}