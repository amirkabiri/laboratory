import React from 'react';

export default function PaperExclamation(props){
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="485.483" height="522.945" viewBox="0 0 485.483 522.945" { ...props }>
            <defs>
                <filter id="Union_2" x="0" y="2.287" width="478.337" height="520.658" filterUnits="userSpaceOnUse">
                    <feOffset dy="63" input="SourceAlpha"/>
                    <feGaussianBlur stdDeviation="28" result="blur"/>
                    <feFlood floodColor="#204ba0" floodOpacity="0.329"/>
                    <feComposite operator="in" in2="blur"/>
                    <feComposite in="SourceGraphic"/>
                </filter>
                <filter id="Path_3853" x="44.5" y="264.806" width="332.413" height="163.637" filterUnits="userSpaceOnUse">
                    <feOffset dy="13" input="SourceAlpha"/>
                    <feGaussianBlur stdDeviation="13" result="blur-2"/>
                    <feFlood floodColor="#204ba0" floodOpacity="0.231"/>
                    <feComposite operator="in" in2="blur-2"/>
                    <feComposite in="SourceGraphic"/>
                </filter>
            </defs>
            <g id="Group_2694" data-name="Group 2694" transform="translate(-302.517 -992)">
                <circle id="Ellipse_115" data-name="Ellipse 115" cx="234" cy="234" r="234" transform="translate(320 992)" fill="#fff" opacity="0.149"/>
                <g id="Group_2211" data-name="Group 2211" transform="translate(0 -47)">
                    <g transform="matrix(1, 0, 0, 1, 302.52, 1039)" filter="url(#Union_2)">
                        <path id="Union_2-2" data-name="Union 2" d="M15749.913,21176.656a42.184,42.184,0,0,1-28.588-11.113,42.136,42.136,0,0,0,28.471,11.113h-211.475a42.318,42.318,0,1,1,0-84.637V20824h268.017v308.578a44.087,44.087,0,0,1-44.079,44.08Zm0-84.637h0Zm-.5,0h0Zm.426,84.637h0Z" transform="translate(-15412 -20800.71)" fill="#fff"/>
                    </g>
                    <g id="Group_2210" data-name="Group 2210" transform="translate(386.517 1062.287)">
                        <g id="paper" transform="translate(0 0)">
                            <g id="Group_2208" data-name="Group 2208" transform="translate(0 0)">
                                <path id="Path_3854" data-name="Path 3854" d="M396.019,96V404.574a44.086,44.086,0,0,1-44.078,44.082H339.594a42.319,42.319,0,1,1,0-84.637H128V96Z" transform="translate(-85.681 -96)" fill="#fff"/>
                                <g transform="matrix(1, 0, 0, 1, -84, -23.29)" filter="url(#Path_3853)">
                                    <path id="Path_3853-2" data-name="Path 3853" d="M304.73,472.965a42.132,42.132,0,0,0,29.182,11.673H122.319a42.319,42.319,0,1,1,0-84.637H333.913a42.319,42.319,0,0,0-29.182,72.965Z" transform="translate(4 -108.69)" fill="#d3d7e2" stroke="#d3d7e2" strokeWidth="1"/>
                                </g>
                            </g>
                        </g>
                        <g id="exclamation" transform="translate(109.483 59.509)">
                            <g id="Group_2306" data-name="Group 2306" transform="translate(57.952 30.991)">
                                <g id="Group_2305" data-name="Group 2305">
                                    <path id="Path_3897" data-name="Path 3897" d="M244.91,128.877a8.91,8.91,0,0,0-8.91,8.91v57.379a8.91,8.91,0,1,0,17.821,0V137.787A8.91,8.91,0,0,0,244.91,128.877Z" transform="translate(-236 -128.877)" fill="#d3d7e2"/>
                                </g>
                            </g>
                            <g id="Group_2308" data-name="Group 2308" transform="translate(54.834 117.963)">
                                <g id="Group_2307" data-name="Group 2307">
                                    <circle id="Ellipse_143" data-name="Ellipse 143" cx="12.029" cy="12.029" r="12.029" fill="#d3d7e2"/>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </svg>
    );
}