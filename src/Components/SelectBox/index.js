import React from 'react';
import styles from "./index.module.scss";
import classNames from 'classnames';

export default function SelectBox({ className, children, ...props }){
    return (
        <select
            className={ classNames('no-outline', styles.selectBox, className) }
            { ...props }
        >{ children }</select>
    );
}