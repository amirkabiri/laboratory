import React from 'react';
import classNames from 'classnames';
import styles from './index.module.scss';
import PropTypes from 'prop-types';
import BluePage from "../BluePage/BluePage";
import Error from "../Icons/Error";
import PaperExclamation from "../Icons/PaperExclamation";

export default function CheckoutErrorPage({ children, title, description, className, ...props }) {
    return (
        <BluePage>
            <div className={ classNames(className, 'container', styles.container) } { ...props }>
                <Error/>
                <h1 className={ styles.title }>{ title }</h1>
                <p className={ styles.description }>{ description }</p>
                <PaperExclamation className={ styles.paperIcon }/>
                { children }
            </div>
        </BluePage>
    );
}

CheckoutErrorPage.defaultProps = {};
CheckoutErrorPage.propTypes = {
    title : PropTypes.string.isRequired,
    description : PropTypes.string.isRequired,
};