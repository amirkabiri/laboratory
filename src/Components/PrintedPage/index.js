import React from 'react';
import BluePage from "../../Components/BluePage/BluePage";
import classNames from 'classnames';
import styles from './index.module.scss';
import ArrowDownIcon from 'Assets/img/arrow-down.svg';
import PrintLogo from 'Assets/img/print-icon.svg';
import PropTypes from 'prop-types';

export default function PrintedPage({ title, children }){
    return (
        <BluePage>
            <div className="container flex flex-column align-items-center">
                <img src={ PrintLogo } alt="logo"/>

                <div
                    style={{ marginTop: 120, marginBottom: 100 }}
                    className={ classNames("header-title", styles.title) }
                >{ title }</div>

                { children }

                <img src={ ArrowDownIcon } alt="arrow icon" className={ styles.arrowDown } />
            </div>
        </BluePage>
    );
}

PrintedPage.propTypes = {
    title : PropTypes.string.isRequired
};