import React from 'react';
import classNames from 'classnames';
import styles from './index.module.scss';
import PropTypes from 'prop-types';
import PaperIcon from 'Assets/img/print-icon.svg';
import BluePage from "../BluePage/BluePage";
import SuccessIcon from 'Assets/img/success-icon.svg';
import ArrowDownIcon from 'Assets/img/arrow-down.svg';

export default function CheckoutSuccessPage({ children, title, description, className, ...props }) {
    return (
        <BluePage>
            <div className={ classNames(className, 'container', styles.container) } { ...props }>
                <img className={ styles.successIcon } src={ SuccessIcon } alt="success icon"/>
                <h1 className={ styles.title }>{ title }</h1>
                <p className={ styles.description }>{ description }</p>
                { children }
                <img className={ styles.paperIcon } src={ PaperIcon } alt="paper icon"/>
                <img className={ styles.arrowIcon } src={ ArrowDownIcon } alt="arrow down icon"/>
            </div>
        </BluePage>
    );
}

CheckoutSuccessPage.defaultProps = {};
CheckoutSuccessPage.propTypes = {
    title : PropTypes.string.isRequired,
    description : PropTypes.string.isRequired,
};