import React, {useEffect, useRef} from 'react';
import styles from './HomeCard.module.css';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

function HomeCard({ title, icon, href, ...props }){
    const cardRef = useRef();

    useEffect(() => {
        if(cardRef.current){
            const { width } = cardRef.current.getBoundingClientRect();

            cardRef.current.style.height = width + 'px';
        }
    }, []);

    return (
        <Link
            to={ href }
            ref={ cardRef }
            className={ classNames('card', styles.card) }
            { ...props }
        >
            <img src={ icon } className={ styles.icon } />
            <span className={ styles.title }>{ title }</span>
        </Link>
    );
}

HomeCard.defaultProps = {
    href : '/'
};
HomeCard.propTypes = {
    href : PropTypes.string,
    title : PropTypes.string,
    icon : PropTypes.elementType
};

export default HomeCard;