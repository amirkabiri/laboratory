import React from 'react';
import SuccessIcon from 'Assets/img/success-icon.svg';
import BluePage from "../BluePage/BluePage";
import styles from './index.module.scss';
import classNames from 'classnames';
import PropTypes from 'prop-types';

export default function SuccessPage({ title, description, className, children, ...props}){
    return (
        <BluePage
            { ...props }
        >
            <div className={ classNames('container', styles.container, className) }>
                <img className={ styles.icon } src={ SuccessIcon } alt="succes iocn"/>
                <h1 className={ styles.title }>{ title }</h1>
                <p className={ styles.description}>{ description }</p>
                <div className={ styles.body }>{ children }</div>
            </div>
        </BluePage>
    )
}

SuccessPage.propTypes = {
    title : PropTypes.string.isRequired,
    description : PropTypes.string.isRequired
};