import React, { useState } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import styles from './Keyboard.module.css';

import PersianLayout from './Persian';

const langLayouts = {
    fa : PersianLayout,
    en : PersianLayout
};

function Keyboard({ className, primaryButtonTitle, open, onChange, ...props }){
    const [lang, setLang] = useState('fa');
    const Layout = langLayouts[lang];

    const onCharClick = ({ target }) => onChange({
        type : 'char',
        value : target.innerText.trim()
    });
    const onSearchClick = () => onChange({
        type : 'search'
    });
    const onSpaceClick = () => onChange({
        type : 'char',
        value : ' '
    });
    const onEscapeClick = () => onChange({
        type : 'escape'
    });

    return (
        <div
            className={ classNames(
                className,
                styles.container,
                { [styles.open] : open }
            ) }
            { ...props }
        >
            <Layout
                primaryButtonTitle={ primaryButtonTitle[lang] }
                onCharClick={ onCharClick }
                onSearchClick={ onSearchClick }
                onSpaceClick={ onSpaceClick }
                onEscapeClick={ onEscapeClick }
                setLang={ setLang }
            />
        </div>
    );
}

Keyboard.defaultProps = {
    open : true,
    onChange : () => null,
    primaryButtonTitle : {
        fa : 'جستجو',
        en : 'search'
    },
};
Keyboard.propTypes = {
    open : PropTypes.bool,
    onChange : PropTypes.func,
};

export default Keyboard;