import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import styles from './Persian.module.css';
import Button from "../Button/Button";
import EscapeIcon from "../Icons/Escape";

function Persian({ setLang, onCharClick, onSearchClick, onSpaceClick, onEscapeClick, primaryButtonTitle }){
    const onLangChange = () => setLang('en');

    const charButtonClassName = classNames(styles.charButton, styles.ripple);
    return (
        <>
            <div className={ styles.row }>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ج</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ح</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>خ</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ه</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ع</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>غ</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ف</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ق</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ث</Button>
            </div>
            <div className={ styles.row }>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ص</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ض</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>گ</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ک</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>م</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ن</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ت</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ا</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ل</Button>
            </div>
            <div className={ styles.row }>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ب</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ی</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>س</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ش</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>چ</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>و</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>پ</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>د</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ز</Button>
            </div>
            <div className={ classNames( styles.row, styles.oneToLastRow ) }>
                <Button onClick={ onEscapeClick } className={ classNames(styles.escapeButton, styles.ripple) }>
                    <EscapeIcon/>
                </Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>?</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>.</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ر</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ز</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ژ</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ط</Button>
                <Button onClick={ onCharClick } className={ charButtonClassName }>ظ</Button>
            </div>
            <div className={ classNames(styles.row, styles.lastRow) }>
                <Button onClick={ onSearchClick } color="success" className={ styles.searchButton }>{ primaryButtonTitle }</Button>
                <Button onClick={ onSpaceClick } className={ classNames(styles.spaceButton, styles.ripple) }>فضای خالی</Button>
                <Button onClick={ onLangChange } className={ classNames(styles.changeLangButton, styles.ripple) }>A B C</Button>
            </div>
        </>
    );
}

Persian.defaultProps = {
    setLang : () => null,
    onCharClick : () => null,
    onSearchClick : () => null,
    onSpaceClick : () => null,
    onEscapeClick : () => null,
};
Persian.propTypes = {
    setLang : PropTypes.func,
    onCharClick : PropTypes.func,
    onSearchClick : PropTypes.func,
    onSpaceClick : PropTypes.func,
    onEscapeClick : PropTypes.func,
    primaryButtonTitle : PropTypes.string.isRequired
};

export default Persian;