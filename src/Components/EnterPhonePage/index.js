import React, {useRef, useState} from 'react';
import BluePage from "Components/BluePage/BluePage";
import NumericKeyboard from "Components/NumericKeyboard/NumericKeyboard";
import Input from "Components/Input/Input";
import Button from "Components/Button/Button";
import PropTypes from 'prop-types';
import validatePhone from '../../Shared/validatePhone';

export default function EnterPhonePage({ title, validation, placeholder, ...props }){
    const phoneNumberRef = useRef();
    const [touched, setTouched] = useState(false);
    const [invalid, setInvalid] = useState(false);

    const onSubmit = () => {
        const { value } = phoneNumberRef.current;


        switch (validation) {
            case 'phone':
                if(!validatePhone(value)){
                    setTouched(true);
                    setInvalid(true);
                    return;
                }
                break;

            case 'static-phone':
                if(value.trim().length === 0){
                    setTouched(true);
                    setInvalid(true);
                    return;
                }
                break;
        }

        if(props.onSubmit){
            props.onSubmit();
        }
    };

    return (
        <BluePage>
            <div className="container">
                <div className="header-title">{ title }</div>

                <Input
                    ref={ phoneNumberRef }
                    placeholder={ placeholder }
                    style={{ marginTop : 70, marginBottom : 40 }}
                    autoFocus={ true }
                    invalid={ touched && invalid }
                />

                <div className="container flex justify-content-center" style={{ marginTop : 70 }}>
                    <NumericKeyboard
                        inputRef={ phoneNumberRef }
                    />
                </div>

                <div className="flex justify-content-center" style={{ marginTop : 70 }}>
                    <Button
                        disabled={ false }
                        onClick={ onSubmit }
                        color="success"
                    >تایید</Button>
                </div>
            </div>
        </BluePage>
    );
}

EnterPhonePage.defaultProps = {
    title : 'شماره همراه خود را وارد کنید',
    placeholder : 'شماره همراه',
    validation : 'phone'
};
EnterPhonePage.propTypes = {
    title : PropTypes.string,
    placeholder : PropTypes.string,
    validation : PropTypes.oneOf(['phone', 'static-phone'])
};