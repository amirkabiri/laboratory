import React, { useRef, useState } from 'react';
import BluePage from "../../Components/BluePage/BluePage";
import NumericKeyboard from "../../Components/NumericKeyboard/NumericKeyboard";
import Input from "../../Components/Input/Input";
import Button from "../../Components/Button/Button";
import PropTypes from 'prop-types';
import validateNationalCode from '../../Shared/validateNationalCode';

function ReservationInfoPage({ onSubmit }){
    const [activeInput, setActiveInput] = useState();
    const acceptingNumberRef = useRef();
    const nationalNumberRef = useRef();
    const invalidInitial = {
        nationalCode : false,
        acceptingNumber : false
    };
    const [invalid, setInvalid] = useState(invalidInitial);

    const onInputFocus = ref => () => {
        setActiveInput(ref);
    };
    const onClick = () => {
        setInvalid(invalidInitial);
        const nationalCode = nationalNumberRef.current.value;
        const acceptingNumber = acceptingNumberRef.current.value;

        if(!validateNationalCode(nationalCode)){
            return setInvalid(invalid => ({
                ...invalid,
                nationalCode : true
            }));
        }

        onSubmit(acceptingNumber, nationalCode);
    };

    return (
        <BluePage>
            <div className="container">
                <div className="header-title">شماره پذیرش و کدملی خود را وارد کنید</div>

                <Input
                    onFocus={ onInputFocus(acceptingNumberRef) }
                    ref={ acceptingNumberRef }
                    placeholder="شماره پذیرش"
                    style={{ marginTop : 70, marginBottom : 40 }}
                    autoFocus={ true }
                    invalid={ invalid.acceptingNumber }
                />
                <Input
                    onFocus={ onInputFocus(nationalNumberRef) }
                    ref={ nationalNumberRef }
                    placeholder="کدملی"
                    invalid={ invalid.nationalCode }
                />

                <div className="container flex justify-content-center" style={{ marginTop : 70 }}>
                    <NumericKeyboard
                        inputRef={ activeInput }
                    />
                </div>

                <div className="flex justify-content-center" style={{ marginTop : 70 }}>
                    <Button onClick={ onClick } color="success">تایید</Button>
                </div>
            </div>
        </BluePage>
    );
}

ReservationInfoPage.defaultProps = {
    onSubmit : () => null
};
ReservationInfoPage.propTypes = {
    onSubmit : PropTypes.func
};

export default ReservationInfoPage;