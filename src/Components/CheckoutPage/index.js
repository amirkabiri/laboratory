import React from 'react';
import BluePage from "../../Components/BluePage/BluePage";
import CheckoutIcon from "../../Components/Icons/Checkout";
import ArrowBottom from "../../Components/Icons/ArrowBottom";
import styles from './index.module.scss';
import classNames from 'classnames';
import PropTypes from 'prop-types';

export default function CheckoutPage({ children, title, description, cost }){
    return (
        <BluePage>
            <div className="flex flex-column align-items-center">
                <div className={ classNames('container', styles.texts) }>
                    <h1 className={ styles.title }>{ title }</h1>
                    <p className={ styles.description }>{ description }</p>
                </div>

                <div className="container">
                    <div className={ styles.costCard }>
                        <span>مبلغ قابل پرداخت</span>
                        <div>
                            <span>{ cost }</span>
                            ریال
                        </div>
                    </div>
                </div>

                { children }

                <div className={ styles.icons }>
                    <ArrowBottom className={ styles.arrowIcon }/>
                    <CheckoutIcon className={ styles.checkoutIcon }/>
                </div>
            </div>
        </BluePage>
    );
}

CheckoutPage.defaultProps = {
    title : 'پرداخت کنید ...',
    description : 'با کشیدن کارت بانکی در دستگاه POS پرداخت خود را تکمیل کنید'
};
CheckoutPage.propTypes = {
    title : PropTypes.string,
    description : PropTypes.string,
    cost : PropTypes.string.isRequired
};