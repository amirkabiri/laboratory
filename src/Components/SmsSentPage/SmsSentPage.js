import React from 'react';
import BluePage from 'Components/BluePage/BluePage';
import SuccessIcon from 'Assets/img/success-icon.svg';
import SmsIcon from 'Assets/img/sms-2-icon.svg';
import styles from './SmsSentPage.module.css';
import classNames from 'classnames';
import PropTypes from 'prop-types';

function SmsSentPage({ title, description }){
    return (
        <BluePage>
            <div className={ classNames('container flex flex-column align-items-center', styles.container) }>
                <img src={ SuccessIcon } alt="success icon"/>

                <div className={ styles.title }>{ title }</div>

                <p className={ styles.description }>{ description }</p>

                <img src={ SmsIcon } alt="sms icon"/>
            </div>
        </BluePage>
    );
}

SmsSentPage.defaultProps = {

};
SmsSentPage.propTypes = {
    title : PropTypes.string.isRequired,
    description : PropTypes.string.isRequired
};

export default SmsSentPage;