import React, {useRef, useState} from 'react';
import styles from './Header.module.css';
import HeaderTop from './HeaderTop';
import SearchInput from '../SearchInput/SearchInput';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Keyboard from "../Keyboard/Keyboard";

export default function Header({ children, className, search, title, button, containerClass }){
    const searchRef = useRef();
    const [keyboardOpen, setKeyboardOpen] = useState(false);

    const onKeyboardChange = e => {
        if(!searchRef || !searchRef.current) return;
        const input = searchRef.current;

        switch (e.type) {
            case 'char':
                input.value += e.value;
                if(search.onChange) search.onChange(input.value);
                break;

            case 'escape':
                input.value = input.value.slice(0, input.value.length - 1);
                if(search.onChange) search.onChange(input.value);
                break;

            case 'search':
                if(search.onSubmit) search.onSubmit(input.value);
                setKeyboardOpen(false);
                break;
        }
    };
    const onSearchInputFocus = () => setKeyboardOpen(true);

    return (
        <>
            <header
                className={ classNames(styles.header, className) }
                style={{ marginBottom : search ? 150 : 50 }}
            >
                <HeaderTop button={ button }/>
                <h1 className="header-title" style={{ paddingBottom : search ? 0 : 50 }}>{ title }</h1>

                { children }

                {
                    search ? (
                        <div className={ containerClass }>
                            <SearchInput
                                onFocus={ onSearchInputFocus }
                                ref={ searchRef }
                            />
                        </div>
                    ) : null
                }
            </header>

            {
                search ? (
                    <Keyboard
                        open={ keyboardOpen }
                        onChange={ onKeyboardChange }
                    />
                ) : null
            }
        </>
    );
}

Header.defaultProps = {
    search : {},
    containerClass : 'container',
};
Header.propTypes = {
    search : PropTypes.object,
    containerClass : PropTypes.string,
    title : PropTypes.string.isRequired,
};