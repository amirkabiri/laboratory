import React from 'react';
import styles from "./HeaderTop.module.css";
import LogoIcon from "../../Assets/img/logo-icon.png";
import HomeIcon from '../../Assets/img/home-icon.svg';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

function HeaderTop({ button }){
    return (
        <div className={ styles.headerTop }>
            <div className={ styles.logo }>
                <img src={ LogoIcon }/>
                <div>
                    <span>آزمایشگاه و پاتولوژی</span>
                    <strong>فوق تخصصی پرنیان</strong>
                </div>
            </div>
            <Link to={ button.href } className={ styles.button }>
                <img src={ button.icon } className={ styles.buttonIcon }/>
                { button.title }
            </Link>
        </div>
    );
}

HeaderTop.defaultProps = {
    button : {
        title : 'خانه',
        icon : HomeIcon,
        href : '/'
    }
};
HeaderTop.propTypes = {
    button : PropTypes.shape({
        title : PropTypes.string,
        icon : PropTypes.elementType,
        href : PropTypes.string
    })
};

export default HeaderTop;