import React from 'react';
import styles from './Input.module.css';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const Input = React.forwardRef(({ className, style, invalid, ...props }, ref) => (
    <div
        className={ classNames(className, styles.container, { [styles.error] : invalid }) }
        style={ style }
    >
        <input
            ref={ ref }
            className={ classNames(styles.input) }
            { ...props }
        />
    </div>
));

Input.defaultProps = {
    invalid : false
};
Input.propTypes = {
    invalid : PropTypes.bool
};

export default Input;