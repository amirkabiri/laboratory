import React from 'react';
import SmsIcon from 'Assets/img/sms-icon.svg';
import classNames from 'classnames';
import styles from './SmsCard.module.css';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

function SmsCard({ icon, title, href, description, className, ...props }){
    return (
        <Link
            to={ href }
            className={ classNames('card', styles.container, className) }
            { ...props }
        >
            <img src={ icon } className={ styles.icon } />
            <div className={ styles.messages }>
                <h2 className={ styles.title }>{ title }</h2>
                {
                    description ? (
                        <p className={ styles.description }>{ description }</p>
                    ) : null
                }
            </div>
        </Link>
    );
}

SmsCard.defaultProps = {
    title : 'ارسال از طریق پیامک',
    href : '/',
    icon : SmsIcon
};
SmsCard.propTypes = {
    title : PropTypes.string,
    description : PropTypes.string,
    href : PropTypes.string,
    icon : PropTypes.elementType
};

export default SmsCard;