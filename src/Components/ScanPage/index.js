import React from 'react';
import BluePage from "../../Components/BluePage/BluePage";
import QrcodeIcon from 'Assets/img/qrcode-icon.svg';
import styles from './index.module.scss';
import ArrowDownIcon from 'Assets/img/arrow-down.svg';
import PropTypes from 'prop-types';

export default function ScanPage({ children, title }){
    return (
        <BluePage>
            <div className="container flex flex-column align-items-center">
                <img src={ QrcodeIcon } alt="qrcode icon"/>

                <div className={ styles.title }>{ title }</div>

                { children }

                <img className={ styles.arrowDown } src={ ArrowDownIcon } alt="arrow down icon"/>
            </div>
        </BluePage>
    );
}

ScanPage.propTypes = {
    title : PropTypes.string.isRequired,
};