import React, {useState} from 'react';
import classNames from 'classnames';
import styles from './Button.module.css';
import PropTypes from 'prop-types';

function Button({ children, className, color, ...props }){
    const [active, setActive] = useState(false);

    const onDown = () => setActive(true);
    const onUp = () => {
        setTimeout(() => setActive(false), 400);
    };

    return (
        <button
            className={ classNames(
                className,
                styles.button,
                styles[color]
            ) }
            { ...props }
            onMouseDown={ onDown }
            onMouseUp={ onUp }
        >
            <div
                className={ classNames(styles.ripple, { [styles.rippleActive] : active, [styles.rippleTransition] : active }) }
            />
            { children }
        </button>
    );
}

Button.defaultProps = {
    color : 'default',
};
Button.propTypes = {
    color : PropTypes.oneOf(['default', 'success', 'blue'])
};

export default Button;