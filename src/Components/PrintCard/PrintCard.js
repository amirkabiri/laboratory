import React from 'react';
import PrinterIcon from 'Assets/img/printer-icon.svg';
import classNames from 'classnames';
import styles from './PrintCard.module.css';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

function PrintCard({ href, title, description, className, ...props }){
    return (
        <Link
            to={ href }
            className={ classNames('card', styles.container, className) }
            { ...props }
        >
            <img src={ PrinterIcon } className={ styles.icon } />
            <div className={ styles.messages }>
                <h2 className={ styles.title }>{ title }</h2>
                {
                    description ? (
                        <p>{ description }</p>
                    ) : null
                }
            </div>
        </Link>
    );
}

PrintCard.defaultProps = {
    title : 'چاپ بر روی کاغذ',
    href : '/'
};
PrintCard.propTypes = {
    title : PropTypes.string,
    description: PropTypes.string,
    href : PropTypes.string
};

export default PrintCard;