import React from 'react';
import styles from './ReservationAlert.module.css';
import ReservationIcon from 'Assets/img/reservation-icon.svg';
import { Link } from 'react-router-dom';

function ReservationAlert(){
    return (
        <Link to="/reservation/reserve" className={ styles.container }>
            <img src={ ReservationIcon } alt=""/>
            <div className={ styles.messages }>
                <h3 className={ styles.title }>نوبت رزرو دارم!</h3>
            </div>
        </Link>
    );
}

export default ReservationAlert;