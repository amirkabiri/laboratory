import React from 'react';
import HeaderTop from '../../Components/Header/HeaderTop';
import styles from './BluePage.module.css';
import PropTypes from 'prop-types';

function BluePage({ header, children }){
    return (
        <div className={ styles.container }>
            {
                header ? (
                    <HeaderTop/>
                ) : null
            }

            { children }
        </div>
    );
}

BluePage.defaultProps = {
    header : true
};
BluePage.propTypes = {
    header : PropTypes.bool
};

export default BluePage;